# Standard Postfix class for most systems.
#   class base::postfix: outgoing email server only
#   class base::postfix::server: incoming and outgoing email server

# Standard Postfix class for outgoing servers.  There is no daemon listening
# on SMTP port.  The outgoing mail will be masqueraded except root mail.
class base::postfix {
  package { 'postfix': ensure => present }

  # The core service.  The init script appears to support status on all the
  # operating systems we care about.  Define a custom reload action so that
  # we can just signal the service when we change configuration files
  # without stopping and restarting the daemons, since Postfix can re-read
  # all its configuration files with a reload.
  service { 'postfix':
    ensure    => running,
    enable    => true,
    hasstatus => true,
    restart   => '/usr/sbin/postfix reload',
    require   => [
      File['/etc/postfix/main.cf'],
      File['/etc/postfix/master.cf'],
      Package['postfix']
    ],
  }

  # We want the CentOS version of master.cf to be the same as the RedHat
  # version:
  $mastercfsuffix = $::osfamily
  #if ($::operatingsystem =~ /(?i:centos)/) {
  #  $mastercfsuffix = 'RedHat'
  #} else {
  #  $mastercfsuffix = $::operatingsystem
  #}

  # Basic configuration files.
  file {
    '/etc/filter-syslog/postfix':
      source  => 'puppet:///modules/base/postfix/etc/filter-syslog/postfix';
    '/etc/postfix/main.cf':
      source  => $::lsbdistcodename ? {
        # rhel5 only fix for prng_exch
        'Tikanga' => 'puppet:///modules/base/postfix/etc/postfix/main.cf.tikanga',
        default   => 'puppet:///modules/base/postfix/etc/postfix/main.cf',
      },
      notify  => Service['postfix'],
      require => Package['postfix'];
    '/etc/postfix/master.cf':
      source  => "puppet:///modules/base/postfix/etc/postfix/master.cf.$mastercfsuffix",
      notify  => Service['postfix'],
      require => Package['postfix'];
  }
  base::postfix::map {
    '/etc/postfix/recipients':
      ensure  => present;
    '/etc/postfix/senders':
      ensure  => present;
    '/etc/postfix/transport':
      source  => 'puppet:///modules/base/postfix/etc/postfix/transport';
  }

  # Have mail from root@stanford.edu come from root at the local system
  base::postfix::sender { 'root@stanford.edu':
    ensure => "root@${::fqdn}";
  }

  # We used to disable and stop sendmail here on Red Hat, but this doesn't
  # work if sendmail is not installed.  Since we'd rather not have sendmail
  # installed, just assume that people are doing this manually.
}
