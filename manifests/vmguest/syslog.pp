# Rename the syslog rotation cron job so that it runs first.  This ensures
# that it will complete before midnight even when other cron jobs are slow, as
# is often the case on VMs.

class base::vmguest::syslog inherits base::newsyslog {
  file { '/etc/cron.daily/newsyslog': ensure => absent; }
  file { '/etc/cron.daily/00newsyslog':
    ensure => present,
    mode   => '0755',
    owner  => 'root',
    source => 'puppet:///modules/base/vmguest/etc/cron.daily/00newsyslog';
  }
}