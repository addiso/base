# modules/iptables/manifests/fragment.pp - definition for base::iptables::fragments ()
#
# Install or remove an iptables fragment.  All fragments should be located in
# the iptables module so that we have some central management of them.

define base::iptables::fragment(
  $ensure,
  $source  = 'NOSRC',
  $content = 'NOCONTENT'
) {
    $realname = "/etc/iptables.d/$name"
    $codename = "iptables::fragment"
    $basefile = "puppet:///modules/base/iptables/fragments/$name"

    case $ensure {
        present: {
            case $content {
                'NOCONTENT': {
                    case $source {
                        'NOSRC': {
                            # pull from the default fragment table
                            $src = $basefile
                        }
                        default: {
                            # source has been specified without content
                            $src = $source
                        }
                    }
                    file { "$realname":
                        source => "$src",
                        notify => Exec["rebuild-iptables"],
                    }
                }
                default: {
                    case $source {
                        'NOSRC': {
                            # content has been specified without source
                            $src = $source
                        }
                        default: {
                            fail "$codename - source or content, not both."
                        }
                    }
                    file { "$realname":
                        content => "$content",
                        notify => Exec["rebuild-iptables"],
                    }
                }
            }
        }

        absent: {
            file { "$realname":
                ensure => absent,
                notify => Exec["rebuild-iptables"];
            }
        }

        default: {
            fail "Invalid ensure value: $ensure"
        }
    }
}
