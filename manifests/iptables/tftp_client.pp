# iptables configuration for servers which want to run tftp client

class base::iptables::tftp_client inherits base::iptables {
  case $lsbdistcodename {
    # squeeze uses a different name
    'squeeze': {
      base::os::kernel_module { "nf_conntrack_tftp":
        ensure => present,
      }
    }
    default: {
      base::os::kernel_module { "ip_conntrack_tftp":
        ensure => present,
      }
    }
  }
}
