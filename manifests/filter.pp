#
# Filters a file, making the desired change to any matching line.  Takes
# three arguments: the file to affect, the regex to find the line to be
# replaced, and the value that the part of the line matching the regex
# should have.  Only makes the change if that line is both present and
# doesn't already have that value.
#
# This uses a sed separator of % and therefore assumes that neither the
# regex nor the value contain %.  We're also currently assuming that the
# value contains no regex metacharacters.
#
# Example:
#    base::filter { "/etc/default/apache2":
#        regex => "NO_START=0",
#        value => "NO_START=1"
#    }

define base::filter($regex, $value, $file="UNSPECIFIED") {
    $fname = $file ? {
        UNSPECIFIED => $name,
        default     => $file
    }

    exec { "filter $name $value":
        command => "sed -i -e 's%$regex%$value%' '$fname'",
        unless  => "grep '$value' '$fname'",
        onlyif  => "grep '$regex' '$fname'"
    }
}