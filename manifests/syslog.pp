##############################################################################
# Base syslog support
##############################################################################
#
# This module uses rsyslog where possible otherwise syslog is used.
#
# The default rsyslog support preserves the use of /etc/syslog.conf.
# The two most common overrides to this function are to not use
# /etc/syslog.conf configuration and to not use the default configuration
# that writes to /var/log/messages.  For example:
#
#  class s_ldap::conf::syslog_dev inherits base::syslog {
#    Base::Syslog::Config::Rsyslog['/etc/rsyslog.conf'] {
#      use_syslog_conf => false,
#      use_default     => false,
#    }
#  }
#
# Notes:
# * The newsyslog service is responsible for ensuring the file permissions
#   for the /var/log/messages log file
# * When use_syslog_conf is set to false then the /etc/syslog.confg file
#   is still installed, but it is ignored.  The goal is to drop the file
#   at some point.

class base::syslog(
  $use_logsink_server = true,
)
{

  # Ubuntu uses a separate account for syslog
  if ( $::operatingsystem == 'Ubuntu' ) {
    $syslog_owner = 'syslog'
    $syslog_group = 'adm'
  } else {
    $syslog_owner = 'root'
    $syslog_group = 'root'
  }

  base::syslog::config::syslog { '/etc/syslog.conf':
    ensure => present,
    owner  => $syslog_owner,
    group  => $syslog_group,
  }

  # RHEL4/5 are ancient and use syslog, everything modern uses rsyslog
  if ( $::osfamily == 'RedHat' and $::lsbmajdistrelease < 6 ) {
    service { 'syslog': ensure => running }
  } else {
    file { '/etc/rsyslog.d':
      ensure => directory,
      mode   => '0755';
    }
    service { 'syslog':
      ensure    => running,
      name      => 'rsyslog',
      hasstatus => true,
    }
    base::syslog::config::rsyslog { '/etc/rsyslog.conf':
      ensure             => present,
      owner              => $syslog_owner,
      group              => $syslog_group,
      use_logsink_server => $use_logsink_server,
    }
  }

  # Create a symlink from /var/log/syslog to /var/log/messages for the
  # poor infrastructure folks used to the old path.
  file { '/var/log/syslog':
    ensure => link,
    target => '/var/log/messages',
  }

  # Install filter-syslog rules to ignore the restart messages from the
  # syslog daemon.
  file { '/etc/filter-syslog/syslog':
    source => 'puppet:///modules/base/syslog/etc/filter-syslog/syslog',
  }

  # Install filter-syslog rules to ignore some messages from
  # systemd.
  file { '/etc/filter-syslog/systemd':
    source => 'puppet:///modules/base/syslog/etc/filter-syslog/systemd',
  }
}
