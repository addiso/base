# Rules specific to Debian systems.  Try to keep this rule set to an absolute
# minimum.  Part of the goal of Puppet is to make our systems look as similar
# as possible given the inherent differences between the distributions, and
# that means that changes should be wrapped in conceptual packages that do
# equivalent things on both distributions.  This should hold only those things
# that configure a Debian OS as such, as distinct from Red Hat.

# We install filter-syslog rules, so make sure that newsyslog is always
# installed.
#
# PARAMETERS
# ----------
#
# $apt_cache_notin_tmp: If you want apt to use a directory other than
# /tmp for its temporary cache, set this parameter to "true". This is needed
# if the /tmp partition is set to "noexec" (apt often needs to execute
# configuration files as part of an install or uninstall).
#
# If $apt_cache_notin_tmp is set to "true" and $apt_cache_directory is
# undefined, then apt will use /var/cache/apt/tmp for its temporary
# directory. If you want to specify a different directory, set
# the $apt_cache_tmp_dir.
#
# $apt_cache_tmp_dir: if $apt_cache_notin_tmp is set to "false" this
# parameter is ignored. If $apt_cache_notin_tmp is set to "true" then we
# configure apt to use $apt_cache_tmp_dir as apt's temporary directory
# during package installs and uninstalls.
#
# NOTE! If you use $apt_cache_tmp_dir to override the default, then you
# must have $apt_cache_tmp_dir as a file resource in your own Puppet
# code, otherwise Puppet will fail.
#
# $debian_stanford_backports: if set to true (the default) will include in
# apt sources the debian-stanford backports repository. There are some
# packages in the debian-stanford backports repository that overlap the
# regular debian repository, so some users may not want to use this extra
# source; in that case, they should set this parameter to false.
# Default: true

class base::os::debian (
  $apt_cache_notin_tmp       = false,
  $apt_cache_tmp_dir         = '/var/cache/apt/tmp',
  $debian_stanford_backports = true
){
  include base::newsyslog

  # Do all of the apt configuration in a separate stage, to ensure that it
  # completes before anything else.
  stage { 'aptitude':
    before => Stage['main'],
  }
  class { 'base::os::debian::apt':
    stage               => 'aptitude',
    apt_cache_notin_tmp => $apt_cache_notin_tmp,
    apt_cache_tmp_dir   => $apt_cache_tmp_dir,
  }

  # Define an aptitude update exec that will only get run if it receives a
  # notification. This is a _convenience_ resource as it will NOT receive a
  # notification from any resource in _this_ class; it is intended that
  # _other_ classes notify it.
  exec { 'aptitude update':
    command     => 'aptitude update',
    path        => '/bin:/usr/bin',
    refreshonly => true,
  }

  # Install basic configuration files.
  file {
    '/etc/default/rcS':
      source => 'puppet:///modules/base/os/etc/default/rcS';
    '/etc/filter-syslog/debian':
      source => 'puppet:///modules/base/os/etc/filter-syslog/debian';
  }

  # Bring in common packages.
  package {
    'bsd-mailx':       ensure => present;
    'dmidecode':       ensure => present;
    'debconf-utils':   ensure => present;
    'locate':          ensure => present;
    'lsb-release':     ensure => present;
    'kstart':          ensure => present;
    'stanford-server': ensure => present;
  }

  # libstdc++5 and smbios-utils are required for Dell firmware updates, so
  # install them on physical machines.
  if $::virtual == 'vmware' {
    package {
      'libsmbios-bin': ensure => absent;
      'smbios-utils':  ensure => absent;
    }
  } else {
    include base::libstdc::v5

    package { 'libsmbios-bin': ensure => absent }
    package { 'smbios-utils':
      ensure  => present,
      require => Package['libsmbios-bin'],
    }
  }

  # For i686 systems, install the optimized version of glibc.
  if $::hardwaremodel == 'i686' {
    package { 'libc6-i686': ensure => present }
  }

  # Handle additional distribution-specific tweaks, usually related to the
  # default package set.
  case $::lsbdistcodename {
    'wheezy': { package { 'emacs23-nox': ensure => present } }
    'jessie': { package { 'emacs24-nox': ensure => present } }
  }

  # Ensure this file exists, containing only a comment.
  file { '/etc/default/locale':
    ensure  => present,
    replace => false,
    content => "# Not used but required by PAM.\n",
  }

  # Our old daemontools build assumed /service, but the new packages
  # available from Debian use /etc/service.  Make the latter a symlink to
  # the former so that we don't have to move everything.
  file {
    '/etc/service':
      ensure => link,
      target => '/service';
    '/service':
      ensure => directory,
      mode   => '0755';
  }

  # allow non-root users to use ping in Jessie
  if ($::lsbdistcodename == 'jessie') {
    exec { 'setcap ping':
      path    => "/usr/bin:/usr/sbin:/bin:/sbin",
      command => 'setcap cap_net_raw+ep /bin/ping',
      unless  => "getcap /bin/ping | grep -q 'cap_net_raw+ep'",
    }
  }
}


# base::os::debian::apt has all of the Puppet code to set up our package
# sources, preferences, etc.  It is meant to be run in its own phase, before
# the main phase.  This is needed so that we can install packages without the
# possibility of weird aptitude errors.

# There is an exec resource in this class that does an 'aptitude
# update'. This class runs in the early 'aptitude' stage (see earlier in
# this file) and hence the exec resource also runs in this early
# stage. However, this can cause a problem if classes running in the
# 'main' stage call 'exec aptitude'. To avoid this issue, we give the
# 'aptitude update' resource in this class a different name; any other
# class that wants to run 'aptitude update' will have to declare the
# 'aptitude update' exec resource itself.

class base::os::debian::apt (
  $apt_cache_notin_tmp = false,
  $apt_cache_tmp_dir   = '/var/cache/apt/tmp'
){

  # Define aptitude update as a command we can run
  exec { 'apt config aptitude update':
    command     => 'aptitude update',
    path        => '/bin:/usr/bin',
    refreshonly => true,
  }

  # Install basic apt configuration files.
  # Make sure this gets created BEFORE `aptitude update` runs.
  file { '/etc/apt/apt.conf.d/10recommends':
    source => 'puppet:///modules/base/os/etc/apt/apt.conf.d/10recommends',
    before => Exec['apt config aptitude update'],
  }

  # If aptitude is using something other than /tmp, set that up first
  if ($apt_cache_notin_tmp) {
    # If we did NOT override the apt cache directory make sure that
    # '/var/cache/apt/tmp' exists.
    if ($apt_cache_tmp_dir == '/var/cache/apt/tmp') {
      file { $apt_cache_tmp_dir:
        ensure  => directory,
      }
    }

    # Create the configuration file to set our apt temp directory
    # Make sure this gets created BEFORE `aptitude update`.
    file { '/etc/apt/apt.conf.d/apt_cache_tmp':
      content => template('base/os/etc/apt/apt.conf.d/apt_cache_tmp.erb'),
      require => File[$apt_cache_tmp_dir],
      before  => Exec['apt config aptitude update'],
    }
  }

  # On wheezy, for right now we have to disable pdiffs due to problems with
  # the Translation files.
  if $::lsbdistcodename == 'wheezy' {
    file { '/etc/apt/apt.conf.d/30no-pdiffs':
      source => 'puppet:///modules/base/os/etc/apt/apt.conf.d/30no-pdiffs',
      before => Exec['apt config aptitude update'],
    }
  }

  # Install APT sources configuration.  This is generally handled via
  # templates.
  file {
    '/etc/apt/sources.list':
      content => template('base/os/sources/sources.list.erb'),
      notify  => Exec['apt config aptitude update'];
    '/etc/apt/sources.list.d':
      ensure  => 'directory',
      recurse => true,
      purge   => true,
      notify  => Exec['apt config aptitude update'];
    '/etc/apt/sources.list.d/backports.list':
      content => template('base/os/sources/backports.list.erb'),
      notify  => Exec['apt config aptitude update'];
    '/etc/apt/sources.list.d/stanford.list':
      content => template('base/os/sources/stanford.list.erb'),
      notify  => Exec['apt config aptitude update'];
  }

  # Install APT preferences.  We should never use /etc/apt/preferences
  # since the preferences.d directory is supported
  file { '/etc/apt/preferences.d':
    ensure  => directory,
    recurse => true,
    purge   => true,
  }
  if $::lsbdistcodename == 'wheezy' {
    file { '/etc/apt/preferences.d/rsyslog':
      content => template('base/os/preferences/rsyslog.erb'),
      notify  => Exec['apt config aptitude update'],
    }
  }
  file {
    '/etc/apt/preferences':
      content => '',
      notify  => Exec['apt config aptitude update'];
    '/etc/apt/preferences.d/backports':
      content => template('base/os/preferences/backports.erb'),
      notify  => Exec['apt config aptitude update'];
  }

  # Install the stanford-keyring package
  package { 'stanford-keyring':
    require         => File['/etc/apt/sources.list.d/stanford.list'],
    notify          => Exec['apt config aptitude update'],
  }
}
