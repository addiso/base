# Allows users to add extra text to /etc/motd by overriding the variable
# 'text' (which is used in the template).  Used as so:
#
#   class s_timeshare::motd inherits base::os {
#       Base::Os::Motd["/etc/motd"] { text => template("base/s_timeshare/motd.erb") }
#   }

define base::os::motd(
  $ensure,
  $template,
  $text = ''
) {
  file { $name:
    ensure  => $ensure,
    content => template($template),
  }
}