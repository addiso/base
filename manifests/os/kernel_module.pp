#
# Definition for loading kernel modules.
# This is taken directly from:
#  http://reductivelabs.com/trac/puppet/wiki/Recipes/KernelModules

define base::os::kernel_module ($ensure) {
    $modulesfile = $osfamily ? {
        'Debian' => '/etc/modules',
        'RedHat' => '/etc/rc.modules',
    }

    case $osfamily {
        'RedHat': {
            file { '/etc/rc.modules':
                ensure => file,
                mode => 755
            }
        }
    }
    case $ensure {
        present: {
            exec { "insert_module_${name}":
                command => $osfamily ? {
                    'Debian' => "/bin/echo '${name}' >> '${modulesfile}'",
                    'RedHat' => "/bin/echo '/sbin/modprobe ${name}' >> '${modulesfile}'",
                },
                unless => "/bin/grep -q '${name}' '${modulesfile}'"
            }
            exec { "/sbin/modprobe ${name}":
                unless => "/bin/grep -q '^${name} ' '/proc/modules'"
            }
        }
        absent: {
            exec { "/sbin/modprobe -r ${name}":
                onlyif => "/bin/grep -q '^${name} ' '/proc/modules'"
            }
            exec { "remove_module_${name}":
                command => $osfamily ? {
                    'Debian' => "/usr/bin/perl -ni -e 'print unless /^\\Q${name}\\E\$/' '${modulesfile}'",
                    'RedHat' => "/usr/bin/perl -ni -e 'print unless /^\\Q/sbin/modprobe ${name}\\E\$/' '${modulesfile}'",
                },
                onlyif => $osfamily ? {
                    'Debian' => "/bin/grep -qFx '${name}' '${modulesfile}'",
                    'RedHat' => "/bin/grep -q '^/sbin/modprobe ${name}' '${modulesfile}'",
                },
            }
        }
        default: { err ( "unknown ensure value ${ensure}" ) }
    }
}
