# RHEL/CentOS7-specific things
# Summary:
#    - Proactively install Perl prerequisites for stanford-* packages
#    - Get around the AFS byte-range issue with puppetized configs
#    - Disable firewalld in favor of iptables
class base::os::redhat::el7 {

  if ($::lsbmajdistrelease == '7') {

    # dependencies to run the stanford-* tools
    $el7_deps = [ 'perl-Config-Simple', 'perl-Crypt-PasswdMD5',
                  'perl-Date-Calc', 'perl-IPC-Run', 'perl-Sys-Hostname-Long',
                  'perl-File-Tail', 'sysstat', 'perl-Net-SNMP',
                  'perl-Net-Server', 'perl-IO-Multiplex', 'perl-News-Article',
                  'perl-Perl6-Slurp' ]
    package { $el7_deps: ensure => present }

    # directory to allow one-time execs
    file { '/var/puppet':
      ensure => 'directory',
      mode   => '0644',
    }

    # remove versions of AFS cell configs installed by openafs-*
    # Due to an encoding 'bug' in Puppet, Puppet cannot checksum the AFS
    # cell configs provided by openafs* because Puppet encodes via ASCII 
    # and chokes on a unicode character in the provided file
    exec { 'workaround_bytelock_afs_error':
      path    => "/usr/bin:/usr/sbin:/bin:/sbin",
      command => 'rm /usr/vice/etc/CellServDB && rm /usr/vice/etc/CellServDB.dist && touch /var/puppet/afs_bytelock_files_removed',
      creates => '/var/puppet/afs_bytelock_files_removed',
      require => File[ '/var/puppet' ],
    }

    # disable firewalld in favor of iptables
    exec { 'disable_firewalld':
      path    => "/usr/bin:/usr/sbin:/bin:/sbin",
      command => 'systemctl mask firewalld ; systemctl stop firewalld ; touch /var/puppet/firewalld_disabled',
      creates => '/var/puppet/firewalld_disabled',
      require => File[ '/var/puppet' ],
    }

    # allow direct interfacing with iptables
    package { 'iptables-services':
      ensure  => installed,
      require => File[ '/var/puppet' ],
    }

    # enable iptables {now, and at boot}
    exec { 'enable_iptables':
      path    => "/usr/bin:/usr/sbin:/bin:/sbin",
      command => 'systemctl enable iptables ; systemctl start iptables ; touch /var/puppet/iptables_enabled',
      creates => '/var/puppet/iptables_enabled',
      require => Package[ 'iptables-services' ],
    }

    # enable afs-client {onboot, and now}
    exec { 'enable_openafs_client_onboot':
      path    => "/usr/bin:/usr/sbin:/bin:/sbin",
      command => 'systemctl enable openafs-client && touch /var/puppet/openafs_client_autostart_enabled',
      creates => '/var/puppet/openafs_client_autostart_enabled',
      require => Package[ 'openafs-client' ],
    }
    exec { 'start_openafs_client_first_time':
      path    => "/usr/bin:/usr/sbin:/bin:/sbin",
      command => 'systemctl start openafs-client && touch /var/puppet/openafs_client_started_first_time',
      creates => '/var/puppet/openafs_client_started_first_time',
      require => Package[ 'openafs-client' ],
    }

    # Our EL7 build of openafs-client does not pull this in automatically
    # opting to fix here rather than change shared afs module
    package { 'openafs-krb5':
      ensure  => installed,
      require => Package[ 'openafs-client' ],
    }

    # Warning about the directory facilitating these fixes
    file { '/var/puppet/README':
      ensure  => present,
      mode    => '0644',
      require => File['/var/puppet'],
      content =>
"This directory is used to hold lock (state) files by Puppet. This directory
and all contents are provided by modules/base/os/redhat/el7.pp. Removing
files in this directory will trigger restarts of critical system services.\n",
    }

  } 
}
