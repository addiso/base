# Since some production RHEL6 hosts (e.g. zm01) have 
# overrides to the syslog.conf file, that part of this
# class is being left commented out for now.
#
# RHEL6 ships with rsyslog 5. This is dumb. Newer versions
# are not available from EPEL either. This leaves 3 options:
#   - settle for V5 (thus, this class),
#   - package a version for placement in Stanford repos,
#   - install the vendor-provided RPMs locally
# All three options have their disadvantages. For now,
# we will put our trust in what RHEL packaged.
#
class base::os::redhat::syslog inherits base::syslog {

  # assuming only affected on RHEL6 for now
  if ($::lsbmajdistrelease == '6') {
    Base::Syslog::Config::Rsyslog['/etc/rsyslog.conf'] {
      use_v5           => 'true',
      use_syslog_conf => 'true',
      use_default     =>  'false',
    }
    
    # if these files happen to be present, rsyslog will really
    # complain upon restart
    $rsyslog_files = [ '/etc/rsyslog.d/20-templates.conf',
                    '/etc/rsyslog.d/95-default.conf',
                    '/etc/rsyslog.d/postfix.conf' ]
    file { $rsyslog_files: ensure => absent } 

    # uncomment to enforce a 'sane' global syslog.conf
    #Base::Syslog::Config::Syslog['/etc/syslog.conf'] {
    #  ensure => present,
    #  source => 'puppet:///modules/s_rpmbuildserver/etc/syslog.conf',
    #}
  }

}
