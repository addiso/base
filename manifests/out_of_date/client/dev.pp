# DEPRECATED!
#
# Use instead base::out_of_date::client with the parameter 'platform' set
# to 'development'.

class base::out_of_date::client::dev {
  class { 'base::out_of_date::client':
    platform => 'development',
  }
}
