# Puppet client that will only list changed lines without context in puppet log
class base::puppetclient::neat inherits base::puppetclient {
  Base::Puppetclient::Config['/etc/puppet/puppet.conf'] {
    diff_args => '--unified=0',
  }
}
