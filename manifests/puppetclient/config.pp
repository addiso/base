# Configuration for a Puppet client.  Handles the Puppet configuration and
# syslog filtering rules.

# replace: set to false if you don't want Puppet to overwrite an existing
#   /etc/puppet/puppet.conf.
#
#
# certname: if you need to override the certname in the [agent] section
# puppet.conf file, set it here. For example, on an AWS machine the
# certname will be different than the AWS public IP hostname.
#
# Helper define to generate Puppet configuration files.
define base::puppetclient::config(
  $ensure,
  $template    = 'base/puppetclient/puppet.conf.template.erb',
  $runinterval = '',
  $server      = '',
  $ca_server   = '',
  $in_noop     = false,
  $pm          = false,
  $start       = true,
  $replace     = true,
  $defaultfile = '/etc/default/puppet',
  $is_master   = false,
  $diff_args   = '-u',
  $certname    = ''
) {

  $ssldir = $::osfamily ? {
    'Debian' => '/etc/puppet/ssl',
    'RedHat' => '/var/lib/puppet/ssl',
  }

  case $ensure {
    present: {
      if ($is_master) {

        # The Puppet config file
        $puppet_config = '/etc/puppet/puppet.conf'

        file { $puppet_config:
          content => template('base/puppetclient/puppet.conf.template.erb'),
          mode    => '0644',
          owner   => 'root',
          group   => 'root',
        }

      } else {
        # not $is_master
        file { $name:
          content => template($template),
          replace => $replace,
        }
      }
    }
    absent:  { file { $name: ensure => absent } }
    default: { fail "Invalid ensure value: ${ensure}" }
  }

  file { $defaultfile:
    content => template('base/puppetclient/puppet.default.erb'),
  }

}

