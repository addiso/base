## 2015-02-24 - Adoption into Stanford Puppet Shared Base

#### Housekeeping

-Pulled from rom GitHub with git clone https://github.com/CERIT-SC/puppet-yum.git
-Renamed to yumtools from puppet-yum for clarity
-Renamed nested classes as base::yumtools::*
-Removed Git-related files and directories
-Removed tests, other cruft

## 2014-12-08 - Release 0.9.4

Fix file/directory permissions.

#### Bugfixes

- Fix PF module archive file/directory permissions.

## 2014-11-06 - Release 0.9.3

Enable yum.conf plugins if disabled.

#### Bugfixes

- Enable yum.conf plugins (if disabled) when we
  install plugin via yum::plugin.

## 2014-09-02 - Release 0.9.2

Fix metadata.json

#### Bugfixes

- Fix metadata.json module dependencies

## 2014-08-20 - Release 0.9.1

### Summary

Fix GPG key import check when key is specified in $content.

#### Bugfixes

- Fix GPG key import check when key is specified in $content.

## 2014-08-07 - Release 0.9.0

### Summary

Initial release.
