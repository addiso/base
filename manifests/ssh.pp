# Sets up an OpenSSH server with an appropriate configuration.  We need to
# support a few configuration variations depending on the vintage of the
# system, we lock connections down to campus with iptables by default, and we
# have a few subclasses that allow things like host keys.

# $pam_afs: If you are using AFS, then you can have pam_afs_session placed
# into the PAM authentication chain.  You should disable this on systems
# that aren't using OpenAFS.
# Default: true

# If you want to require Duo on login, set pam_duo to true. This flag will
# load the appropriate Duo code (via base::duo) and change the sshd_config
# file so that Duo is required for non-root logins. If you want Duo for
# sudo, see the base::sudo class.
# Default: false

# $ip_ranges: An array of iptables-compatible IP addresses that are
# allowed to access port 22 on this server.
# Default:
#    [
#      '10.32.0.0/15',
#      '10.34.0.0/15',
#      '10.36.0.0/15',
#      '10.39.0.0/16',
#      '10.48.0.0/17',
#      '171.64.0.0/14',
#      '172.16.0.0/12',
#      '192.168.0.0/16',
#      '204.63.224.0/21'
#    ]
# (This is historical.)

# $filter_sunetids: ignore "authentication failure" messages for this list
# of sunetids.
# Default: the empty array (so don't filter any such messages)

# $pubkey: set to true if you want to allow ssh key-pair logins to this
# server.
# Default: false

# $root_authorized_keys: Set this to a Puppet template URL to
# instantiate that file as /root/.ssh/authorized_keys.
# This is especially useful for Kerberos KDCs that
# are not clients of the production KDC. Use with caution.
# If you set this to true you should also set $pubkey to true.
#
# Example:
#    root_authorized_keys => 'mymodule/root/.ssh/authorized_keys.erb',
#
# Default: undef

# $extra_skip_duo_users: See documentation in base::ssh::config::sshd.
# Default: []

class base::ssh(
  $pam_afs               = true,
  $pam_duo               = false,
  $pam_slurm             = false,
  $ip_ranges             =
    [
      '10.32.0.0/15',
      '10.34.0.0/15',
      '10.36.0.0/15',
      '10.39.0.0/16',
      '10.48.0.0/17',
      '171.64.0.0/14',
      '172.16.0.0/12',
      '192.168.0.0/16',
      '204.63.224.0/21'
    ],
  $pubkey                  = false,
  $root_authorized_keys    = undef,
  $filter_sunetids         = [],
  $extra_skip_duo_users    = [],
){

  # Install the openssh server package.
  class {'base::ssh::package':
    pam_duo => $pam_duo,
  }

  # If we are using Duo, then bring in our Duo config.  We want GECOS off.
  # This also brings in Duo packages.
  if $pam_duo {
    base::duo::config { '/etc/security/pam_duo_ssh.conf':
      ensure    => present,
      use_gecos => false,
    }
  }

  # If we are using SLURM, install the module.
  if $pam_slurm {
    package { 'libpam-slurm':
      ensure => installed,
    }
  }

  # Setup /etc/pam.d/sshd to require Duo on regular logins.
  class { 'base::ssh::pam':
    pam_afs   => $pam_afs,
    pam_duo   => $pam_duo,
    pam_slurm => $pam_slurm,
  }

  # Our default ssh rules allow connections from all of campus.  This is
  # mostly for legacy reasons, since it historically had always done this and
  # we weren't sure what would break.
  #
  # Individual groups should provide their own local override class that
  # restricts remctl to their bastion host and/or subnets from which their
  # administrators do work and use that class on most of their systems.
  base::iptables::rule { 'ssh':
    protocol => 'tcp',
    port     => '22',
    source   => $ip_ranges,
  }

  # Ensure the daemon is running.
  service { 'ssh':
    ensure  => running,
    name    => $::osfamily ? {
      'Debian' => 'ssh',
      'RedHat' => 'sshd',
    },
    require => Package['openssh-server'],
  }

  # Install ssh (client) configuration file.
  base::ssh::config::ssh { '/etc/ssh/ssh_config':
    ensure => present,
    notify => Service['ssh'],
  }

  if ($root_authorized_keys) {
    file { '/root/.ssh/authorized_keys':
      ensure  => present,
      content => template($root_authorized_keys),
      mode    => '0640',
    }
  } else {
    # Make sure public key authentication to root does not work and clean up
    # after the authorized_keys file generated during the build process.  Some
    # clients (HPC) will need to override this (for GPFS, for example).
    file {
      '/root/.ssh/authorized_keys':  ensure => absent;
      '/root/.ssh/authorized_keys2': ensure => absent;
    }
  }

  # If we have $root_authorized_keys defined, then this means we want to
  # allow logins to the root account with an ssh key-pair. So, set
  # $rootlogin_with_keypair appropriately and use it in the
  # base::ssh::config::sshd define below.
  if ($root_authorized_keys) {
    $rootlogin_with_keypair = true
  } else {
    $rootlogin_with_keypair = false
  }

  # Install sshd (server) configuration file.
  base::ssh::config::sshd { '/etc/ssh/sshd_config':
    ensure                 => present,
    pam_duo                => $pam_duo,
    pubkey                 => $pubkey,
    rootlogin_with_keypair => $rootlogin_with_keypair,
    extra_skip_duo_users   => $extra_skip_duo_users,
    notify                 => Service['ssh'],
  }

  # Ignore routine ssh messages.
  file { '/etc/filter-syslog/ssh':
    content => template('base/ssh/etc/filter-syslog/ssh.erb'),
  }
}
