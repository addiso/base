# Installs newsyslog, the program that we use for log rotation, and installs
# standard configuration and disables logrotate and the default system log
# rotation.  Also installs filter-syslog, which we use for auditing system
# logs, and its basic configuration.

class base::newsyslog {

  # Install the basic filter-syslog system
  include base::filtersyslog

  package {'newsyslog':
    ensure => present;
  }

  # Disable logrotate, since otherwise it will fight with newsyslog.  We could
  # try to remove it, but we keep running into programs that depend on it and
  # make the removal unnecessarily difficult.
  file { '/etc/cron.daily/logrotate': ensure => absent }

  # Determine the PID file location for the HUP action in the default messages
  # configuration.  Red Hat uses syslogd and Debian uses rsyslogd.
  case $::osfamily {
    'RedHat': { $pid_file = '/var/run/syslogd.pid'  }
    default:  { $pid_file = '/var/run/rsyslogd.pid' }
  }

  # Determine the owner.  Ubuntu makes the syslog user owner of all of the
  # logs; everyone else uses root.
  case $::lsbdistname {
    'ubuntu': { $log_owner = 'syslog' }
    default:  { $log_owner = 'root'   }
  }

  # Default log rotation rules for /var/log/messages.
  base::newsyslog::config { 'messages':
    frequency => 'daily',
    log_owner => $log_owner,
    log_mode  => '640',
    analyze   => '/usr/bin/filter-syslog',
    logs      => [ 'messages' ],
    restart   => "hup ${pid_file}",
  }

  # btmp permissions must be 600 in RHEL systems
  # sshd on RHEL systems will complain otherwise, since bad ssh attempts
  # often are the result of entering a password as a username
  $btmp_perms = $osfamily ? {
    'RedHat' => '600',
    default  => '660',
  }

  # Rotate btmp and wtmp monthly and save one year's worth of those files.
  # This requires two separate log configurations because there isn't a way to
  # represent different permissions for different files in
  # base::newsyslog::config currently.
  base::newsyslog::config { 'btmp':
    frequency => 'monthly',
    log_owner => 'root',
    log_group => 'utmp',
    log_mode  => $btmp_perms,
    logs      => [ 'btmp' ],
    save_num  => '12',
  }
  base::newsyslog::config { 'wtmp':
    frequency => 'monthly',
    log_owner => 'root',
    log_group => 'utmp',
    log_mode  => '664',
    logs      => [ 'wtmp' ],
    save_num  => '12',
  }

  # Bad login attempt logging is only done if btmp exists.  Ensure the file is
  # present with the correct permissions.
  file { '/var/log/btmp':
    ensure => file,
    owner  => 'root',
    group  => 'utmp',
    mode   => $btmp_perms,
  }

  # Remove an old misspelled configuration file.
  file { '/etc/newsyslog.monthly/wmtp': ensure => absent }

  # Clean up after old Puppet manifests.  We used to install a weekly
  # newsyslog configuration to tar up the root .history-save directory and
  # save it, but now we no longer rotate root history.  Delete the lingering
  # newsyslog configuration if it exists.
  file { '/etc/newsyslog.weekly/audit': ensure => absent }
}
