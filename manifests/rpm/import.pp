
define base::rpm::import($url, $signature) {
    exec { $name:
        command => "/bin/rpm --import $url",
        unless => "/bin/rpm -qi $signature"
    }
}