# Enable remctl on port 4444.  This is for systems that need to support
# ancient remctl clients, primarily the old Java client used by Administrative
# Systems MaIS applications.

# We have to clean up the legacy supervise job first before we install the
# xinetd job.
class base::remctl::legacy inherits base::remctl {
  if $operatingsystem == 'Debian' {
    base::daemontools::supervise { 'remctld': ensure => absent }
  }

  # Install the xinetd configuration.  Force a PATH setting since remctld
  # scripts may be assuming they have a sane PATH but xinetd may not have
  # any PATH set.
  base::xinetd::config { 'remctl-legacy':
    server      => '/usr/sbin/remctld',
    description => 'Remote authenticated command server',
    server_type => 'UNLISTED',
    port        => '4444',
    cps         => '100 5',
    env         => 'PATH=/sbin:/bin:/usr/sbin:/usr/bin',
    require     => Base::Daemontools::Supervise['remctld'],
  }

  # Add port 4444 to the remctl iptables rule.
  Base::Iptables::Rule['remctl'] { port +> [ 4444 ] }
}