# This class handles setting up the tripwire client.

# tripwire is installed in AFS so we only need the cron job and the
# filter-syslog rules from the remctl interface installed by stanford-server.
class base::tripwireclient {
    file {
        '/etc/cron.daily/tripwire':
            source => 'puppet:///modules/base/tripwireclient/etc/cron.daily/tripwire',
            mode   => '0755';
        '/etc/filter-syslog/tripwire':
            source => 'puppet:///modules/base/tripwireclient/etc/filter-syslog/tripwire';
        '/etc/tripwire':
            ensure => directory,
            mode   => '0755';
        '/etc/tripwire/config':
            source => 'puppet:///modules/base/tripwireclient/etc/tripwire/config';
    }
}
