#   class base::postfix::server: incoming and outgoing email server

# Standard Postfix server configuration that allows both incoming and
# outgoing mail.  Override things selectively so that we accept mail from
# the network and deliver mail locally.
class base::postfix::server inherits base::postfix {
  
  $mastercfsuffix = $::osfamily

  File['/etc/postfix/master.cf'] {
    source => "puppet:///modules/base/postfix/etc/postfix/master-server.cf.$mastercfsuffix",
  }
  File['/etc/postfix/main.cf'] {
    source => 'puppet:///modules/base/postfix/etc/postfix/main-server.cf',
  }

  # Allow incoming mail from anywhere.
  base::iptables::rule { 'smtp':
    description => 'Allow incoming SMTP traffic from anywhere',
    protocol    => 'tcp',
    port        => [ 25, 465, 587 ],
  }
}
