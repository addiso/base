#
# Example of how to set a value in sysctl.conf
#   base::sysctl { "kernel.sysreq": ensure => "0"  }
#
# Example of how to ensure there is no value in sysctl.conf
#   base::sysctl { "kernel.sysreq": ensure => absent  }
#
# The structure of this module is a little odd since the definition is the
# top-level object and any uses in manifests are classes, but there's nothing
# else that makes much sense to have as the sysctl top-level class.
# Eventually this should become a native type, which would resolve all these
# issues and let us role the TCP keepalive configuration into os.  Leave it as
# is for right now.

define base::sysctl($ensure) {
    include base::sysctl::file

    $filename = "/etc/sysctl.conf"
    case $ensure {
        absent: {
            exec { "rm-sysctl-$name":
                path    => '/bin:/usr/sbin:/usr/bin',
                command => "sed -i -e '/^$name/d' $filename",
                onlyif  => "grep '^[^#]' $filename | grep ^$name"
            }
        }
        default: {
            $line = "$name = $ensure"
            exec { "add-sysctl-$name":
                path    => '/bin:/usr/sbin:/usr/bin',
                command => "echo '$line' >> $filename",
                unless  => "grep '^$name' $filename",
                notify  => Exec["reload sysctl.conf"]
            }
            exec { "fix-sysctl-$name":
                path    => '/bin:/usr/sbin:/usr/bin',
                command => "sed -i -e '/^$name/d' $filename; echo '$line' >> $filename",
                unless  => "grep '^$name[[:space:]]*=[[:space:]]*$ensure' $filename",
                require => Exec["add-sysctl-$name"],
                notify  => Exec["reload sysctl.conf"]
            }
        }
    }
}

# Generic resources used by the definition to avoid repeating resources if
# we change multiple settings.
class base::sysctl::file {
    file { "/etc/sysctl.conf":
        owner => "root",
        group => "root",
    }

    # Reload sysctl values after configuration is changed.
    exec { "reload sysctl.conf":
        command     => "/sbin/sysctl -p /etc/sysctl.conf",
        refreshonly => true
    }
}

# Tune kernel tcpkeepalive values to be shorter than
# the 30 minutes firewall session timeout value
class base::sysctl::tcp_keepalive {
    base::sysctl {
        "net.ipv4.tcp_keepalive_intvl":  ensure => 60;
        "net.ipv4.tcp_keepalive_probes": ensure => 20;
        "net.ipv4.tcp_keepalive_time":   ensure => 600;
    }
}
