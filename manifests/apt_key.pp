#
# Adds GPG keys to apt-key

class base::apt_key {
    exec { 'add-apt-key':
        command     => 'cat /etc/keyrings/* | /usr/bin/apt-key add -',
        refreshonly => true,
    }

    file { '/etc/keyrings':
        ensure  => directory,
        purge   => true,
        recurse => true,
        notify  => Exec['add-apt-key'],
    }
}
