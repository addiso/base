# Installs a version of the sshd_config file which maps the gitolite user to
# a script that will use GSS-API to determine the exact calling user.  This
# is currently Debian-only.

class base::ssh::gitolite inherits base::ssh {
  Base::Ssh::Config::Sshd['/etc/ssh/sshd_config'] { gitolite => true }
}