# Create the ssh client configuration.
#
# Set protocol_one to true to allow the client to fall back to protocol
# version one (required for some networking devices).

define base::ssh::config::ssh(
  $ensure       = 'present',
  $source       = undef,
  $protocol_one = false,
) {
  if $source {
    $template = undef
  } else {
    $template = template('base/ssh/ssh_config.erb')
  }
  file { $name:
    ensure  => $ensure,
    source  => $source,
    content => $template,
  }
}