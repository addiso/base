# Allow public key authentication to the server, required by some of our
# clients.
class base::ssh::pubkey inherits base::ssh {
  Base::Ssh::Config::Sshd['/etc/ssh/sshd_config'] { pubkey => true }

  # Ignore the public key authentications when filtering syslog.
  file { '/etc/filter-syslog/ssh-pubkey':
    source => 'puppet:///modules/base/ssh/etc/filter-syslog/ssh-pubkey',
  }
}