# Install or remove an rsyslog fragment.  Recommented practice is to
# include fragments in the syslog module, but they can be pulled from
# any Puppet manifest.  The default is use Puppet templates for
# fragments, which allows dynamic content without having to define all
# possible substitutions as part of the define.  Some default values
# are provided; for example, syslog_target defaults to
# logsink.stanford.edu.
#
# Example: Direct syslog output to the logsink-dev server using the
#          base module template 50-tcp-output.conf.erb.
#
#   syslog_target = 'logsink-dev.stanford.edu'
#   base::syslog::fragment { '50-tcp-output.conf': ensure => present }
#
# Example: Use the puppet template 90-default-remote.conf.erb from the
#          base module and the local file 95-local.conf from the
#          s_audit puppet manifest.
#
#   base::syslog::fragment {
#     '90-default-remote.conf':
#       ensure => present;
#     '95-local.conf':
#       ensure => present,
#       source => 'puppet:///modules/s_audit/etc/rsyslog.d/95-local.conf';
#   }
#
# Example: Write rsyslog debugging output to the file
#          /var/log/rsyslog-pstats.log.
#
#  base::syslog::fragment {
#    '10-impstats-module.conf': ensure  => 'present';
#    '99-impstats-output.conf': ensure  => 'present';
#  }

define base::syslog::fragment(
  $ensure  = 'present',
  $source  = undef,
  $content = undef
) {
  $realname = "/etc/rsyslog.d/$name"
  $codename = 'syslog::fragment'
  $basetmpl = "base/syslog/etc/rsyslog.d/${name}.erb"

  # Useful default template values
  if $syslog_target {
    $syslog_server = $syslog_target
  } else {
    $syslog_server = 'logsink.stanford.edu'
  }

  # If both source and content are set, error out.
  if ($source != undef) and ($content != undef) {
    fail('Specify source or content but not both')
  }

  # If there is no source and no content, we provide a default content value.
  if ($source == undef) and ($content == undef) and ($ensure == 'present') {
    $content_real = template($basetmpl)
  } else {
    $content_real = $content
  }

  # Install the file.
  file { $realname:
    ensure  => $ensure,
    content => $content_real,
    source  => $source,
    notify  => Service['syslog'],
  }
}
