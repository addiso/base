# Install or remove an rsyslog fragment that supports TLS/RELP
# connections.
#
# The tricky part of setting up a TLS/RELP connection between a client
# and a syslog server is getting the required certificates installed.
# See the ikiwiki documentation for help in generating the client
# certificates.
#
# Fragment Example
# ----------------
#
#     if $syslogfacility-text == 'local4' then {
#        action(type="omfile"
#               file="/var/log/ldap"
#               template="FileFormat")
#        action(type="omrelp"
#               name="<%= @cert_subject %>Remote"
#               Target="<%= @syslog_server %>"
#               Port="10515"
#               tls="on"
#               tls.caCert="<%= @ca_cert_file %>"
#               tls.myCert="<%= @client_cert %>"
#               tls.myPrivKey="<%= @client_key %>"
#               tls.authmode="name"
#               tls.permittedpeer=["syslog.<%= @syslog_server %>"]
#               queue.FileName="<%= @cert_subject %>Queue"
#               template="ForwardFormat")
#         stop
#     }
#
# Simple Exmaple
# --------------
#
#     base::syslog::tls_ca_cert: { $::fqdn_lc: ensure => present }
#     base::syslog::tls { '50-ldap-remote.conf':
#         ensure => 'present',
#         content => 's_idg_test/etc/rsyslog.d/50-ldap-tls.conf.erb'),
#     }
#
# Complex Example
# ---------------
#
#  class s_idg_test::zoot_vm2 inherits defaults {
#    include s_idg_test::zoot_vm2::syslog
#  }
#
#  class s_idg_test::zoot_vm2::syslog inherits base::syslog {
#    Base::Syslog::Config::Rsyslog['/etc/rsyslog.conf'] {
#      use_default => false,
#    }
#
#    base::syslog::tls_ca_cert: { $::fqdn_lc:
#      syslog_server => 'logsink-dev.stanford.edu',
#      ensure        => present
#    }
#    base::syslog::tls { '50-ldap-tls.conf':
#      ensure        => 'present',
#      syslog_server => 'logsink-dev.stanford.edu',
#      content       => 's_idg_test/etc/rsyslog.d/50-ldap-tls.conf.erb',
#    }
#    base::syslog::tls { '95-default-tls.conf':
#      ensure        => 'present',
#      syslog_server => 'logsink-dev.stanford.edu',
#      content       => 's_idg_test/etc/rsyslog.d/95-default-tls.conf.erb',
#      install_cert  => false,
#    }
#  }

define base::syslog::tls(
  $ensure        = 'present',
  $content       = undef,
  $syslog_server = 'logsink.stanford.edu',
  $service_name  = undef,
  $install_cert  = true
) {

  include base::syslog::tls_support

  $realname     = "/etc/rsyslog.d/$name"
  $basetmpl     = "base/syslog/etc/rsyslog.d/${name}.erb"
  $ca_cert_file = "/etc/ssl/certs/${syslog_server}.ca.pem"

  # Require a template
  if $content == undef {
    fail('syslog fragment must be specified')
  }

  if $ensure == 'present' {

    # Install the certificate for the service
    if $service_name == undef {
      $cert_subject = "syslog.${::fqdn_lc}"
      $wallet_name  = "ssl-key/${::fqdn_lc}/syslog"
    } else {
      $cert_subject = "syslog.${service_name}"
      $wallet_name  = "ssl-key/${service_name}/syslog"
    }
    if $install_cert == true {
      apache::cert::other { $cert_subject:
        ensure   => present,
        keyname  => $wallet_name,
        identity => "${cert_subject}.pem",
        symlink  => false,
        notify   => Service['syslog'],
      }
    }

    # Install the rsyslog fragment from a template
    $client_cert = "/etc/ssl/certs/${cert_subject}.pem"
    $client_key  = "/etc/ssl/private/${cert_subject}.key"
    file { $realname:
      ensure  => $ensure,
      content => template($content),
      notify  => Service['syslog'],
      require => File["/etc/ssl/certs/${syslog_server}.ca.pem"],
    }

  } else {

    file { $realname: ensure => $ensure }

  }
}
