# This is resource should be defined before the first use of
# base::tls.  The certificate installed is referenced in the rsyslog
# fragments installed by base::tls.
#
# Example:
#
#   base::syslog::tls_ca_cert{ $::fqdn_lc: ensure => present }

define base::syslog::tls_ca_cert(
  $syslog_server = 'logsink.stanford.edu',
  $ensure        = 'present'
) {

  case $ensure {
    'present': {
      # Install the CA certificate for the syslog server
      $ca_cert_file = "${syslog_server}.ca.pem"
      $ca_cert_path = "/etc/ssl/certs/$ca_cert_file"
      file { $ca_cert_path:
        ensure => present,
        source => "puppet:///modules/cert-files/${ca_cert_file}",
        notify => Service['syslog'],
      }
      apache::cert::hash { $ca_cert_file:
        ensure  => present,
        require => File[$ca_cert_path],
      }
    }
    'absent': {
      file { $ca_cert_file: ensure => absent }
    }
  }
}
