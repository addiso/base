#
# Configuration customization for tcsh.  We may want to add root's .cshrc
# here as well at some point, but currently on Debian that's handled by
# stanford-server.

class base::tcsh {
    file    { '/etc/csh.cshrc': source => 'puppet:///modules/base/tcsh/etc/csh.cshrc' }
    package { 'tcsh':           ensure => present  }
}