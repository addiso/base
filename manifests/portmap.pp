#
# Portmap service for Sun RPC.  Needed on systems providing RPC services, such
# as NFS servers, and occasionally for other reasons.

# This class intentionally does not add iptables rules.  You should add with
# server-specific iptables rules to allow only specific sites to talk to port
# 111.
class base::portmap {

  case $::osfamily {
    'RedHat': {
      include base::xinetd

      case $::lsbmajdistrelease {
        '6','7': {
          package { 'rpcbind': ensure => present }
          service { 'rpcbind': ensure => running }
        }
        default: {
          package { 'portmap': ensure => present }

          # Ensure service is running.  Also notify xinetd in case the
          # services need to be re-registered with the portmap.
          service { 'portmap':
            ensure    => running,
            enable    => true,
            hasstatus => true,
            require   => Package['portmap'],
            restart   => '/etc/init.d/portmap restart',
            notify    => Service['xinetd'],
          }
        }# rhel4/5
      }
    }
    'Debian': {
      if $::operatingsystem == 'Ubuntu' {
        # Ubuntu 11.10 switched to "rpcbind" like EL6
        package { 'rpcbind': ensure => present }
        service { 'portmap': ensure => running }
      } else { 
          case $::lsbdistcodename {
            'squeeze': {
              package { 'portmap': ensure => present }
              service{ 'portmap':
                ensure    => running,
                enable    => true,
                hasstatus => false,
                status    => 'pidof portmap',
                require   => Package['portmap'],
                restart   => '/etc/init.d/portmap restart',
              }
            }
            default: {
              package { 'rpcbind': ensure => present }
              service { 'rpcbind': ensure => running }
            }
          }
      }
    }
  }
}
