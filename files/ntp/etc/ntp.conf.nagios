# This is a variation on the default site ntpd configuration file that permits
# time queries from the Nagios servers.  It queries the three stratum-one time
# servers, which use GPS receivers for accurate time.

# Listen to the stratum 2 pool.
server time-a.stanford.edu iburst
server time-b.stanford.edu iburst
server time-c.stanford.edu iburst

# Save the clock drift.
driftfile /var/lib/ntp/ntp.drift

# Only talk to the network where the time servers are and to the Nagios
# servers.
restrict 171.64.7.0 mask 255.255.255.0 nomodify
restrict 171.67.16.36 nomodify
restrict 171.67.22.24 nomodify
restrict 171.67.217.112 mask 255.255.255.240 nomodify
restrict 204.63.224.64 mask 255.255.255.192 nomodify

# Allow all settings from our localhost interface.
restrict 127.0.0.1

# Do not allow access from anybody else
restrict default ignore

# Always reset the clock, even if the new time is more than 1000s away
# from the current system time.  We need this on VMs and it shouldn't hurt
# anywhere else.
tinker panic 0

# Disable the monlist command, since it can be used for a UDP DoS attack.
disable monitor
