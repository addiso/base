# This is the ntpd configuration file for systems we run that aren't on the
# Stanford network, such as the BCDR systems at Duke.  It uses the default
# time servers from a stock Debian installation.

# Use the Debian participants in pool.ntp.org.
server 0.debian.pool.ntp.org iburst
server 1.debian.pool.ntp.org iburst
server 2.debian.pool.ntp.org iburst
server 3.debian.pool.ntp.org iburst

# Save the clock drift.
driftfile /var/lib/ntp/ntp.drift

# By default, exchange time with everybody, but don't allow configuration.
# See /usr/share/doc/ntp-doc/html/accopt.html for details.
restrict -4 default kod notrap nomodify nopeer noquery
restrict -6 default kod notrap nomodify nopeer noquery

# Local users may interrogate the ntp server more closely.
restrict 127.0.0.1
restrict ::1

# Always reset the clock, even if the new time is more than 1000s away from
# the current system time.  We need this on VMs and it shouldn't hurt anywhere
# else.
tinker panic 0

# Disable the monlist command, since it can be used for a UDP DoS attack.
disable monitor
