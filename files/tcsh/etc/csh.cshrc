# /etc/csh.cshrc -- System-wide .cshrc file for csh(1) and tcsh(1)
# $Id: csh.cshrc 4177 2007-12-04 22:15:18Z digant $

# Default to a sane umask.
umask 022

# Needed on Red Hat, should be harmless on Debian.
setenv MAIL "/var/spool/mail/$USER"

set path = (/usr/local/sbin /usr/local/bin /usr/sbin /usr/bin /sbin /bin)

if ($?tcsh && $?prompt) then
    bindkey "\e[1~" beginning-of-line   # Home
    bindkey "\e[7~" beginning-of-line   # Home rxvt
    bindkey "\e[2~" overwrite-mode      # Ins
    bindkey "\e[3~" delete-char         # Delete
    bindkey "\e[4~" end-of-line         # End
    bindkey "\e[8~" end-of-line         # End rxvt

    set autoexpand
    set autolist
    set prompt = "%U%m%u:%B%~%b%# "
endif

# Load customizations on Red Hat.
if (-d /etc/profile.d) then
    set nonomatch
    foreach i (/etc/profile.d/*.csh)
        if (-r $i) then
            source $i
        endif
    end
    unset i nonomatch
endif
