# /etc/filter-syslog/RedHat -- Red Hat syslog filter rules.
# $Id: filter-syslog 21084 2009-12-01 20:59:04Z rra $

# Ignore various messages produced by Red Hat core packages.
anacron:            /^Anacron started/
anacron:            /^Jobs will be executed sequentially$/
anacron:            /^Normal exit/
anacron:            /^(Will run job|Job \`cron\.)/
CRON:               /^\(pam_unix\) session (opened|closed) for user root/
CROND:              /^\(root\) CMD /
/USR/SBIN/CRON:     /^\(root\) CMD /
crond:              /^\(root\) CMD /
crond(pam_unix):    /^session (opened|closed) for user root/
crond:              /^pam_unix\(crond:session\): session (opened|closed) for user root/
crontab:            /^\(root\) LIST \(root\)$/
pam_afs:            /^AFS Ignoring superuser root$/
pam_afs:            /^\(pam_unix\) session (opened|closed) for user root/
PAM_unix:           /^\(cron\) session (opened|closed) for user root/
PAM_unix:           /^\(su\) session opened for user nobody by \(uid=0\)$/
login:              /^pam_unix\((remote|login):session\): session (opened|closed) for user root(| by rlogin\(uid=0\))$/
hwclocksync:        /.*/
kernel:             /^TCP: Treason uncloaked\!/
kernel:             /^KERNEL: assertion \(\!sk->sk_forward_alloc\) failed/
su:                 /^\+ \?\?\? root[:-]nobody ?$/
su:                 /^\(pam_unix\) session opened for user nobody by \(uid=0\)$/
su:                 /^pam_unix\(su:session\): session (opened|closed) for user root(| by .*\(uid=\d+\))$/
pcscd:              /^winscard.c:\d+:SCardConnect\(\) Reader E-Gate 0 0 Not Found$/
auditd:             /^Audit daemon rotating log files$/

run-parts(/etc/cron.hourly):  /^(starting|finished)/
run-parts(/etc/cron.daily):   /^(starting|finished)/
run-parts(/etc/cron.weekly):  /^(starting|finished)/
run-parts(/etc/cron.monthly): /^(starting|finished)/

# ignore rhn normal checks
rhnsd:              /^running program /usr/sbin/rhn_check/

# some RH systems have vmware free installed
kernel:             /^/dev/vmmon.*: host clock rate change request/
vmware-authd:       /^PAM (adding faulty module:|unable to dlopen|\[error:)/

# ignore munin sudo up2date --list
sudo:               /^\s*munin.*COMMAND=/usr/sbin/up2date --list/

# Ignore yum package update messages.
yum:                /^Updated: \S+ - \S+$/
yum:                /^Installed: \S+$/

# Ignore RH specific normal ipmi messages
kernel:             /^ipmi message handler version \d+.\d+$/
kernel:             /^IPMI System Interface driver version/
kernel:             /^ipmi_si: Found SMBIOS-specified state machine/
kernel:             /^\s+IPMI \w+ interface initialized$/
kernel:             /ipmi device interface version \d+.\d+$/
lsb_log_message:    /^\s+succeeded$/
udevd:              /^udev done\!$/

# Ignore RHEL normal mptctl messages
kernel:             /^Fusion MPT base driver/
kernel:             /^Copyright \(c\) \S+-\S+ LSI Corporation$/
kernel:             /^Fusion MPT misc device \(ioctl\) driver/
kernel:             /^mptctl: Registered with Fusion MPT base driver$/
kernel:             /^mptctl: \/dev\/mptctl \@ \(major,minor=10,220\)$/

