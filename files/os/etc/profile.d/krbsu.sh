# $Id: krbsu.sh 1122 2007-03-15 23:30:57Z digant $
#
# Setup the klogin = rlogin -x alias and run aklog

alias klogin="rlogin -x -f"
if [ -x /usr/local/bin/klist ] 
then
    if /usr/local/bin/klist -s 
    then
        /usr/bin/aklog
    fi
elif [ -x /usr/bin/klist ] 
then
    if /usr/bin/klist -s 
    then
        /usr/bin/aklog
    fi
fi
