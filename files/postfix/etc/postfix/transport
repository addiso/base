# Postfix transport definitions.  -*- conf -*-
#
# This file is maintained by Puppet, as part of base::postfix.
# You can override it in your code, if you like!  Just be sure to use the
# base::postfix::map type, to ensure that `postmap` is run when this file
# changes.

# By default, relay all email through the campus mail gateway.
# This is basically the same as using the relayhost option in main.cf,
# but using the transport map gives us more flexibility!
*           smtp:smtp.stanford.edu

# The only time you should remove the above line is (a) you are using this code
# to push to a cloud (or other non-on-Stanford-network system), or (b) your
# system has an exemption to send mail directly to the outside world.
# (Or, you might be like SoM, and have your own mail gateways!)

# If you have a host or domain where you want to use the MX records returned by
# DNS, then use lines like the following:
#stanford.edu		:
#.stanford.edu		:

# ... but!  If you use lines like the above, then you'd have to put in
# additional lines for any sub-domains where you want to use the mail gateway.
#alumni.stanford.edu	smtp:smtp.stanford.edu
#law.stanford.edu	smtp:smtp.stanford.edu
#sls.stanford.edu	smtp:smtp.stanford.edu
